<?php
require_once "frog.php";
require_once "ape.php";

$sheep = new Animal("shaun");
$frog = new Frog("buduk");
$ape = new Ape("kera sakti");


echo $sheep->getName();
echo $sheep->getLegs();
echo $sheep->getColdBlooded();
echo "<br>";
echo $frog->getName();
echo $frog->getLegs();
echo $frog->getColdBlooded();
echo $frog->jump();
echo "<br>";
echo $ape->getName();
echo $ape->getLegs();
echo $ape->getColdBlooded();
echo $ape->yell();
?>