<?php
class Animal{
    public $nama;
    public $legs = 4;
    public $cold_blooded = "no";
    public function __construct($nama){
        $this->nama= $nama;
    }
    public function getName(){
        echo "Nama          : ".$this->nama."<br>";
    }
    public function getLegs(){
        echo "legs           : ".$this->legs."<br>";
    }
    public function getColdBlooded(){
        echo "cold blooded  : ".$this->cold_blooded."<br>";
    }
}
?>